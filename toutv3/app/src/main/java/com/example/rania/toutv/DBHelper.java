package com.example.rania.toutv;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Batychka on 26/02/15.
 */
public class DBHelper extends SQLiteOpenHelper{


    static final String DB_NAME = "toutv";
    static final  int DB_VERSION = 1;

    //table emission
    static   final String E_TABLENAME = "emissions";
    static final String E_ID = "_id";
    static String E_TITRE = "titre_emissions";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table "+E_TABLENAME
                +" ("+E_ID+" primary key, "
                +E_TITRE+" text)";
        db.execSQL(sql);
        Log.d("DB","database create");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + E_TABLENAME);
        onCreate(db);

    }

    public Cursor listeEmissions(SQLiteDatabase db){
        Cursor c = db.rawQuery("select * from"
                    +E_TABLENAME+" order by asc "+E_TITRE+" asc", null);
        Log.d("DB", "liste des emissions");
        return c;
    }
}
