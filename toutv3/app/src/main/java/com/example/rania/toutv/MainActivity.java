package com.example.rania.toutv;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    Button btn;
    ListView listv;
    TextView titre;
    MyAdapter adapter;
    SQLiteDatabase db;
    DBHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        btn = (Button)findViewById(R.id.button);
        listv = (ListView)findViewById(R.id.activity_list);
        titre = (TextView)findViewById(R.id.activity_title);

        titre.setText("Main Activity!");
        btn.setOnClickListener(this);

        dbh = new DBHelper(this);
        db = dbh.getWritableDatabase();
        Cursor c = dbh.listeEmissions(db);

        adapter = new MyAdapter(this,c);
        listv.setAdapter(adapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "Chargement des donnees du Web", Toast.LENGTH_SHORT).show();
        new DownloadWebTask().execute();
    }

    public class DownloadWebTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void  doInBackground(Void... params) {
            ToutvWebAPI web = new ToutvWebAPI();

            ContentValues cv = new ContentValues();
            for (int i=0; i<web.titres.size();i++){
                String t = web.titres.get(i);
                cv.put(DBHelper.E_ID,i);
                cv.put(DBHelper.E_TITRE,t);
                long k = db.insert(DBHelper.E_TABLENAME, null, cv);
                if (k<0){
                    Log.e(DBHelper.E_TABLENAME,"insertion impossible..");
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void v) {
            Cursor c = dbh.listeEmissions(db);
            adapter.changeCursor(c);

        }
    }

    public class MyAdapter extends CursorAdapter{
        LayoutInflater inflater;

        public MyAdapter(Context context, Cursor c){
            super(context, c, true);
            inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if(v==null){
                v = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
            }
            Cursor c = getCursor();
            c.moveToPosition(position);
            TextView titre = (TextView)v.findViewById(android.R.id.text1);

            String t = c.getString(c.getColumnIndex(DBHelper.E_TITRE));
            titre.setText(t);

            return v;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return null;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

        }


    }
}
