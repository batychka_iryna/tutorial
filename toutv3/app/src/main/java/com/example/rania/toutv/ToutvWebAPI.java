package com.example.rania.toutv;

import android.graphics.drawable.Drawable;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by rania on 19/02/15.
 */
public class ToutvWebAPI {
    ArrayList<String> titres;
    ArrayList<Drawable> images;

    public ToutvWebAPI(){
        String url = "http://www.tou.tv/presentation/section/a-z";
        titres = new ArrayList<String>();
        images = new ArrayList<Drawable>();

        try {
            HttpEntity page = getHttp(url);
            String contenu = EntityUtils.toString(page, HTTP.UTF_8);
            JSONObject js = new JSONObject(contenu);
            JSONArray lineups = js.getJSONArray("Lineups");
            JSONObject emissions = lineups.getJSONObject(0);
            JSONArray lineupItems = emissions.getJSONArray("LineupItems");
            Log.d("WEB", "Nombre d'emissions: "+lineupItems.length());

            for(int i =0;i<lineupItems.length();i++){
                JSONObject item = lineupItems.getJSONObject(i);
                String title = item.getString("Title");
                String image_url = item.getString("ImageUrl");
                if(!item.getString("Template").equals("multiple-content"))
                    titres.add(title);
            }
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        } catch (JSONException e) {
            Log.d("JSON ","Erreur: "+e.getMessage());
        }

    }

    public HttpEntity getHttp(String url) throws ClientProtocolException, IOException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet http = new HttpGet(url);
        HttpResponse response = httpClient.execute(http);
        return response.getEntity();
    }

}
